import * as RT from "./RT";
import { Selection } from './utils'

const { m, cxs } = RT.plugins.utils;

export const rotationPlugin = RT.makePlugin("mental-rotation", function (
  rootScreen,
  config,
  rootCallback
) {
  const screen = RT.centeredScreen(rootScreen);

  const p = (x) => "rotation_stimuli/" + x;

  const stimulus = config.stimulus;

  const centeredContainerClass = cxs({
    display: "flex",
    justifyContent: "center",
  });

  const choiceClass = (selected) =>
    cxs({
      border: `5px solid ${selected ? "black" : "white"}`,
      cursor: "pointer",
      margin: "5px",
    });

  const Rotation = () => {
    const selection = new Selection(2);

    return {
      oncreate() {
        selection.start();
      },
      view() {
        return m(
          "div",
          m(
            "div",
            { class: centeredContainerClass },
            m("img", { src: p(stimulus.target_img) })
          ),
          m(
            "p",
            { class: cxs({ textAlign: "center" }) },
            "Pick two of the choices that are the same object."
          ),
          m(
            "div",
            { class: centeredContainerClass },
            stimulus.choices.map((img) =>
              m("img", {
                class: choiceClass(selection.in(img)),
                onclick: () => selection.add(img),
                src: p(img),
              })
            )
          ),
          m(
            "div",
            {
              class:
                centeredContainerClass +
                " " +
                cxs({
                  marginTop: "1rem",
                  ...(!selection.valid() && { visibility: "hidden" }),
                }),
            },
            m(
              "button",
              {
                onclick: () => {
                  if (selection.valid()) {
                    m.cleanup(screen);
                    rootCallback({
                      choices: selection.state(),
                      log: selection.log(),
                      ...selection.watch.stop(),
                    });
                  }
                },
              },
              "Continue"
            )
          )
        );
      },
    };
  };

  m.mount(screen, Rotation);
});
