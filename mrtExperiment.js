import { mrtPlugin } from "./mrtPlugin";
import { spaceToRespond, get } from "./utils";

function makeStimulus(problemSet, itemNumber) {
  return {
    problemSet: problemSet,
    itemNumber: itemNumber,
    target: `${problemSet} - ${itemNumber}_Problem.png`,
    choices: [1, 2, 3, 4, 5, 6, 7, 8].map(
      x => `${problemSet} - ${itemNumber}_${x}.png`
    )
  };
}

function makePracticeStimulus(itemNumber) {
  return {
    problemSet: "Practice",
    itemNumber: itemNumber,
    target: `Practice - ${itemNumber}a_Problem.png`,
    choices: [1, 2, 3, 4, 5, 6, 7, 8].map(
      x => `Practice - ${itemNumber}a_${x}.png`
    )
  };
}

const aStimuli = [];
const bStimuli = [];

for (let i = 0; i < 23; i++) {
  aStimuli.push(makeStimulus("A", i + 1));
  bStimuli.push(makeStimulus("B", i + 1));
}

const practiceStimuli = [];

for (let i = 0; i < 6; i++) {

  // remove practice stimulus 3, because it's weird and trips people up
  if (i != (3 - 1)) {
    practiceStimuli.push(makePracticeStimulus(i + 1));
  }
}

// not adding bstimuli here because of time constraints
const experimentalStimuli = aStimuli;

const INSTRUCTIONS = `
<h2 id="instructions">Instructions</h2>
<p>For each trial, you will now see a series of target images displayed in a 3x3 square with one missing image. You will also be presented with 8 choices images below the 3x3 square.</p>
<p>The target images follow a pattern. Your task will be to determine which of the choices of images presented is the missing image. You can click a choice image to select it as your answer. It will then be shown with a black rectangle around it. You can also change your mind, and click another choice image instead.</p>
<p>After you have made your final choice, click the <strong>Continue</strong> to respond and complete the trial.</p>
<p>We will now move on to practice trials. You will be provided feedback during practice trials. <strong>You will not be provided feedback for the experimental trials</strong>.</p>
`;

// ## Instructions

// For each trial, you will now see a series of target images displayed in a
// 3x3 square with one missing image. You will also be presented with 8 choices
// images below the 3x3 square.
//
//
// The target images follow a pattern. Your task will be to determine which of
// the choices of images presented is the missing image. You can click a choice
// image to select it as your answer. It will then be shown with a black
// rectangle around it. You can also change your mind, and click another choice
// image instead.
//
// After you have made your final choice, click the **Continue** to respond and
// complete the trial.

// We will now move on to practice trials. You will be provided feedback
// during practice trials. **You will not be provided feedback for the
// experimental trials**.

export async function* mrtExperiment() {
  const key = await get("mrt_stimuli/mrtKeyCleaned.json");

  const lookupTable = {};

  function makeKey(problemSet, itemNumber) {
    return `${problemSet}_${itemNumber}`;
  }

  for (const item of key) {
    lookupTable[makeKey(item.problemSet, item.itemNumber)] = parseInt(
      item.correctChoiceNumber
    );
  }

  function isCorrect(trialData, stimulus) {
    return (
      trialData.choiceNumber ===
      lookupTable[makeKey(stimulus.problemSet, stimulus.itemNumber)]
    );
  }

  yield spaceToRespond(INSTRUCTIONS, true);

  for (const stimulus of practiceStimuli) {
    while (true) {
      yield spaceToRespond(
        "<p>Start the next trial.</p>"
      );
      const trialData = yield mrtPlugin({ stimulus });
      if (!isCorrect(trialData, stimulus)) {
        yield spaceToRespond(
          "<p>That was <span style='color: red; font-weight: bold;'>incorrect!</span> Please try again!<p>"
        );
      } else {
        yield spaceToRespond(
          "<p>That was <span style='color: green; font-weight: bold;'>correct!</span> Moving on to the next practice trial.<p>"
        );
        break;
      }
    }
  }

  yield spaceToRespond("<p>The practice trials are now over. We are now moving on to experimental trials.</p>");

  for (const stimulus of experimentalStimuli) {
    yield spaceToRespond(
      "<p>Start the next trial.</p>"
    );
    yield mrtPlugin({ stimulus });
  }
}
