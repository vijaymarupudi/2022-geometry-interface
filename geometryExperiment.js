import { geometryPlugin } from "./geometryPlugin";
import { GEOMETRY_INSTRUCTIONS } from "./constants";
import { cacheImages, get, spaceToRespond } from "./utils";
import * as RT from "@vijaymarupudi/reaction-time";

async function* geometryExperimentRep(trialInfo, condition) {

  const trainingTrialInfo = jsPsych.randomization.shuffle(trialInfo.training);
  const experimentalTrialInfos = trialInfo.experimental;

  yield spaceToRespond(
    GEOMETRY_INSTRUCTIONS(condition == "different" ? "DIFFERENT" : "SIMILAR"),
    true
  );


  while (true) {
    for (const info of trainingTrialInfo) {
      yield geometryPlugin({
        stimulus: info,
        key: "training",
        condition: condition
      });
    }

    const resp = yield RT.plugins.jsPsych({
      type: "html-button-response",
      stimulus:
        "Would you like to practice again? If you understood the task, press continue to move on to the experimental trials.",
      choices: ["Try again", "Continue"]
    });

    if (parseInt(resp.button_pressed) == 1) {
      break;
    }
  }

  yield spaceToRespond("");

  const block1 = jsPsych.randomization.shuffle(experimentalTrialInfos[0]);

  for (const info of block1) {
    yield geometryPlugin({
      stimulus: info,
      key: "geometry",
      block: 1,
      condition: condition
    });
  }

  yield spaceToRespond(
    "<p>You may now take a short break. Press [spacebar] to continue after you are done.</p>"
  );

  const block2 = jsPsych.randomization.shuffle(experimentalTrialInfos[1]);

  for (const info of block2) {
    yield geometryPlugin({
      stimulus: info,
      key: "geometry",
      block: 2,
      condition: condition
    });
  }
}

export async function* geometryExperiment() {
  const stimInfo = await get("geometry_stimuli/stiminfo.json");
  const filenames = stimInfo
    .flatMap(group => group.filenames)
    .map(x => `geometry_stimuli/${x}`);
  // cache all the images

  await cacheImages(filenames);

  const trialInfo = await get("/api/visuospatial/visuospatial-stimuli");

  RT.plugins.jsPsych.init();

  const CONDITION = 'similar'

  yield* geometryExperimentRep(trialInfo, CONDITION)
}
