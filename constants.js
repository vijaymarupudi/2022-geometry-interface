export const GEOMETRY_INSTRUCTIONS = text => `

<h2>Instructions</h2>

<p>In this part of the study, you will be shown one image on the top of your display,
and two images on the bottom. <b>You will have to determine which of the
bottom images is MOST ${text} to the image on the top.</b> One you have decided on your choice, you can press the <b>Z</b> key to pick the left image or the <b>M</b> key to pick the right image.</p>

<p>We will now move on to practice trials</p>
`

export const CONSENT_FORM = `<h2 id="consent-form">Consent Form</h2>
<p>You are invited to participate in a research study of how people think visually and spatially. You were selected as a possible participant because (a) you are an undergraduate student at the University of Minnesota, (b) you are between 18-24 years of age, and (c) you have ACT or SAT scores on file with the registrar. Please read this form and ask any questions you may have before agreeing to be in the study.</p>
<p>This study is being conducted by Vijay Marupudi (graduate student, Educational Psychology Department) and Dr. Sashank Varma (Professor, Educational Psychology Department).</p>
<h2 id="background-information">Background Information</h2>
<p>The purpose of this study is to investigate how people judge the similarities or differences among sets of images.</p>
<h2 id="procedures">Procedures</h2>
<p>If you agree to be in this study, we would ask you to complete four computerized tasks: (1) a perceptual similarity task, (2) a perceptual difference task, (3) a spatial reasoning task, and a (4) a spatial problem solving task.</p>
<p>In the perceptual similarity task, you will view a target image that is composed of straight and curved lines and simple shapes. You will be asked to pick which of two candidate images is “most similar to” the target image.</p>
<p>The perceptual difference task is similar, except that will be asked to pick which of two candidate images is “most different from” the target image.</p>
<p>In the spatial reasoning task, you will be shown a target image depicting a complex three-dimensional shape made up of cubes. You will also be shown 4 candidate images, and be asked to pick the 2 candidate images that depict the same shape as the target image.</p>
<p>In the spatial problem solving task, you will be shown a 3x3 grid of 8 images and 1 empty cell. You will also be shown 8 candidate images. Each image will be a simple combination of lines and shapes. You will be asked to notice patterns across the images, and on this basis to pick which candidate image goes in the missing cell.</p>
<p>Each task will begin with a few practice trials to familiarize you with how the stimuli will be presented and how you should respond. You will be allowed to take short breaks periodically.</p>
<p>The study will take approximately 40-45 minutes to complete. We will ask you to complete it in one continuous session (except for the short breaks).</p>
<h2 id="risks-and-benefits-of-being-in-the-study">Risks and Benefits of being in the Study</h2>
<p>The study has no known risks. However, you might find some of the experimental activities frustrating. You can withdraw from this study at any time and receive partial payment for your time.</p>
<p>There are no expected benefits to participating in this study.</p>
<h2 id="compensation">Compensation:</h2>
<p>You will receive payment in the form of $15 for participating in this study. Your participation is entirely voluntary. You are free to withdraw at any time, and if you do, you will receive partial payment in the form of $5.</p>
<h2 id="confidentiality">Confidentiality</h2>
<p>The records of this study will be kept private. In any sort of report we might publish, we will not include any information that will make it possible to identify a subject. Research records will be stored securely and only researchers will have access to the records.</p>
<h2 id="voluntary-nature-of-the-study">Voluntary Nature of the Study</h2>
<p>Participation in this study is voluntary. Your decision whether or not to participate will not affect your current or future relations with the University of Minnesota. If you decide to participate, you are free to not answer any question or withdraw at any time without affecting those relationships.</p>
<h2 id="contacts-and-questions">Contacts and Questions</h2>
<p>For questions about the research study or results, or about other concerns, please contact the research team members:</p>
<p>Vijay Marupudi<br />
Ph.D. student<br />
612-636-6788<br />
marupudi@umn.edu</p>
<p>Dr. Sashank Varma<br />
Professor<br />
612-625-6718<br />
sashank@umn.edu</p>
<p>This research has been reviewed and approved by an Institutional Review Board (IRB) within the Human Research Protections Program (HRPP). To share feedback privately with the HRPP about your research experience, call the Research Participants’ Advocate Line at 612-625-1650 or go to <a href="http://www.irb.umn.edu/report.html" class="uri">http://www.irb.umn.edu/report.html</a>. You are encouraged to contact the HRPP if:</p>
<ul>
<li>Your questions, concerns, or complaints are not being answered by</li>
<li>the research team.</li>
<li>You cannot reach the research team.</li>
<li>You want to talk to someone besides the research team.</li>
<li>You have questions about your rights as a research participant.</li>
<li>You want to get information or provide input about this research.</li>
</ul>
<p>You will be given a copy of this information to keep for your records at the end of the experiment, if you desire.</p>
<h2 id="statement-of-consent">Statement of Consent</h2>
<p>I have read the above information. I have asked questions and have received answers. I consent to participate in the study.</p>
<p>By signing this form, I also agree to give permission to the investigators to obtain my standardized test scores – all components of my ACT and/or SAT scores – maintained by the University of Minnesota</p>
<p>Full Name: <input name="name" required></p>
<p>Participant number: <input name="participantNumber" required></p>
<p>UMN Student ID (number): <input name="umnStudentId" required></p>
<p>UMN Email: <input name="umnEmail" required></p>
<p>Signature (type full name): <input name="signature" required></p>
<p>Today’s Date (mm/dd/yyyy): <input name="date" required></p>
<p><button>Submit</button></p>`


export const DEMOGRAPHICS_FORM = `<h1 id="demographics-form">Demographics Form</h1>
<p>We are collecting demographic and background information about our participants. These data will be anonymous and confidential. They will not be reported at the individual level, but rather at the group level (e.g., “8% of the participants were left handed”).</p>
<p>Please choose the answer that best applies to you and/or write in your response on the blank line.</p>
<ol type="1">
<li><p>Are you Hispanic or Latin(x)?</p>
<div>
<input type="radio" name="hispanic" id="hispanicYes" value="Yes"><label for="hispanicYes">Yes</label>
</div>
<div>
<input type="radio" name="hispanic" id="hispanicNo" value="No"><label for="hispanicNo">No</label>
</div></li>
<li><p>With which racial category or categories do you most closely identify:</p>
<div>
<input type="radio" name="racialCategories" id="racialCategoriesAmerican Indian / Alaskan Native" value="American Indian / Alaskan Native"><label for="racialCategoriesAmerican Indian / Alaskan Native">American Indian / Alaskan Native</label>
</div>
<div>
<input type="radio" name="racialCategories" id="racialCategoriesAsian" value="Asian"><label for="racialCategoriesAsian">Asian</label>
</div>
<div>
<input type="radio" name="racialCategories" id="racialCategoriesBlack or African American" value="Black or African American"><label for="racialCategoriesBlack or African American">Black or African American</label>
</div>
<div>
<input type="radio" name="racialCategories" id="racialCategoriesNative Hawaiian or Other Pacific Islander" value="Native Hawaiian or Other Pacific Islander"><label for="racialCategoriesNative Hawaiian or Other Pacific Islander">Native Hawaiian or Other Pacific Islander</label>
</div>
<div>
<input type="radio" name="racialCategories" id="racialCategoriesWhite" value="White"><label for="racialCategoriesWhite">White</label>
</div>
<div>
<input type="radio" name="racialCategories" id="racialCategoriesMultiracial" value="Multiracial"><label for="racialCategoriesMultiracial">Multiracial</label>
</div>
<p>If you picked “Multiracial”, please note which categories you identify with: <input type="text" name="racialCategoriesMultiracial"></p></li>
<li><p>With which gender identity do you most closely identify:</p>
<div>
<input type="radio" name="gender" id="genderMale" value="Male"><label for="genderMale">Male</label>
</div>
<div>
<input type="radio" name="gender" id="genderFemale" value="Female"><label for="genderFemale">Female</label>
</div>
<div>
<input type="radio" name="gender" id="genderNon-binary or gender non-confirming" value="Non-binary or gender non-confirming"><label for="genderNon-binary or gender non-confirming">Non-binary or gender non-confirming</label>
</div>
<div>
<input type="radio" name="gender" id="genderPrefer not to say" value="Prefer not to say"><label for="genderPrefer not to say">Prefer not to say</label>
</div>
<div>
<input type="radio" name="gender" id="genderPrefer to self describe" value="Prefer to self describe"><label for="genderPrefer to self describe">Prefer to self describe</label>
</div>
<p>If you picked “Prefer to self describe”, please describe here: <input type="text" name="genderSelfDescribe"></p></li>
<li><p>Are you left or right-handed?</p>
<div>
<input type="radio" name="handedness" id="handednessLeft-handed" value="Left-handed"><label for="handednessLeft-handed">Left-handed</label>
</div>
<div>
<input type="radio" name="handedness" id="handednessRight-handed" value="Right-handed"><label for="handednessRight-handed">Right-handed</label>
</div></li>
<li><p>What is your academic year?</p>
<div>
<input type="radio" name="academicYear" id="academicYearFreshman" value="Freshman"><label for="academicYearFreshman">Freshman</label>
</div>
<div>
<input type="radio" name="academicYear" id="academicYearSophomore" value="Sophomore"><label for="academicYearSophomore">Sophomore</label>
</div>
<div>
<input type="radio" name="academicYear" id="academicYearJunior" value="Junior"><label for="academicYearJunior">Junior</label>
</div>
<div>
<input type="radio" name="academicYear" id="academicYearSenior" value="Senior"><label for="academicYearSenior">Senior</label>
</div></li>
<li><p>What is your birthdate (mm/dd/yyyy)?</p>
<p><input type="text" name="birthdate"></p></li>
<li><p>What was the highest level of math that you took in high school?</p>
<div>
<input type="radio" name="mathLevel" id="mathLevelAlgebra" value="Algebra"><label for="mathLevelAlgebra">Algebra</label>
</div>
<div>
<input type="radio" name="mathLevel" id="mathLevelGeometry with an algebra prerequisite" value="Geometry with an algebra prerequisite"><label for="mathLevelGeometry with an algebra prerequisite">Geometry with an algebra prerequisite</label>
</div>
<div>
<input type="radio" name="mathLevel" id="mathLevelPre-calculus or trigonometry" value="Pre-calculus or trigonometry"><label for="mathLevelPre-calculus or trigonometry">Pre-calculus or trigonometry</label>
</div>
<div>
<input type="radio" name="mathLevel" id="mathLevelCalculus" value="Calculus"><label for="mathLevelCalculus">Calculus</label>
</div>
<div>
<input type="radio" name="mathLevel" id="mathLevelOther (please specify)" value="Other (please specify)"><label for="mathLevelOther (please specify)">Other (please specify)</label>
</div>
<p>If you picked “Other”, please describe here: <input type="text" name="mathOtherDescribe"></p></li>
<li><p>What is your <strong>college</strong> major / minor? (Indicate more than one if applicable.)</p>
<p><input type="text" name="majorMinor"></p></li>
<li><p>Please list your college courses (designator, number, and short title) in <strong>mathematics</strong></p>
<textarea name="mathCourses" cols="80" rows="20"></textarea></li>
</ol>
<p><button>Submit</button></p>`
