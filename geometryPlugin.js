import * as RT from "./RT";

const cxs = RT.plugins.utils.cxs;

function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

const choiceClass = cxs({ margin: "5vh" });
const choiceContainerClass = cxs({ display: "flex", justifyContent: "center" });

function makeImage(filename) {
  const image = new Image();
  image.src = "geometry_stimuli/" + filename;
  image.style = 'width: 23vh; height: 23vh;'
  image.className = choiceClass;
  return image;
}

export const geometryPlugin = RT.makePlugin("geometry", async function(
  rootScreen,
  trial,
  callback
) {

  const screen = RT.centeredScreen(rootScreen);

  const fixationClass = cxs({ fontSize: "3rem" });

  // show images
  screen.innerHTML = `<span class='${fixationClass}'>+</span>`;
  await wait(1000);
  screen.innerHTML = "";
  const mainStimImage = makeImage(trial.stimulus.main_stim);
  const mainStimDiv = document.createElement("div");
  mainStimDiv.className = choiceContainerClass;
  mainStimDiv.appendChild(mainStimImage);
  screen.appendChild(mainStimDiv);

  const p = document.createElement("p");
  p.innerHTML = `Which of these is <b>most ${
    trial.condition
  }</b> to the image above? Press <b>Z for left</b> and <b>M for right</b>.`;
  p.className = cxs({ textAlign: "center" });
  screen.appendChild(p);

  const choicesDiv = document.createElement("div");
  choicesDiv.className = choiceContainerClass;
  const choiceImage1 = makeImage(trial.stimulus.display_stims[0]);
  const choiceImage2 = makeImage(trial.stimulus.display_stims[1]);
  choicesDiv.appendChild(choiceImage1);
  choicesDiv.appendChild(choiceImage2);
  screen.appendChild(choicesDiv);

  const startTimestamp = performance.now();
  const startDateTime = new Date().valueOf();

  const listener = e => {
    const endTimestamp = performance.now();

    let answer;
    switch (e.key) {
      case "z":
        answer = "left";
        break;
      case "m":
        answer = "right";
        break;
      default:
        return;
    }

    const data = {
      stimulus: trial.stimulus,
      rt: endTimestamp - startTimestamp,
      startTime: startDateTime,
      response: answer
    };

    document.removeEventListener("keyup", listener);

    callback(data);
  };

  document.addEventListener("keyup", listener);
});
