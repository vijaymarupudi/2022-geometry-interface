from dataclasses import dataclass, asdict
from typing import List, Tuple
import json

# ident is 1 2 3 4... training1...
# option is one of: a b c d target
def img_file_path(ident, option):
    return str(ident) + "_" + option + ".png"

@dataclass
class Item:
    type: str
    target_img: str
    choices: List[str]
    answers: Tuple[str, str]

def gen_items():
    with open("raw_rotation_answer_key.txt") as f:
        for line in f:
            line = line.strip()
            ident, answers_str = line.split()

            target_img = img_file_path(ident, 'target')
            answers = [img_file_path(ident, opt) for opt in answers_str]
            choices = [img_file_path(ident, opt) for opt in "abcd"]

            yield Item(target_img=target_img, choices=choices, answers=answers, type='training' if ident.startswith('training') else 'experimental')

def main():
    dict_items = [asdict(x) for x in gen_items()]
    training = [x for x in dict_items if x['type'] == 'training']
    experimental = [x for x in dict_items if x['type'] != 'training']
    with open("rotation_stimuli.json", 'w') as f:
        json.dump(dict(training=training, experimental=experimental), f)

main()
