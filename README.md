# README

To build the experiment's interface code, run

```
npm install
npm run build
```

This will compile and move all the necessary files to the `dist/`
folder. **Note that files related to the matrix reasoning task are not
included in this repository.** This is because those files require
signing an agreement for the images of the UCMRT test described by
Pahor et al., 2019. Please contact the authors of the UCMRT paper to
sign the agreement, and then contact the authors of this paper for the
correctly formatted and named files that work with the rest of the
code.

Alternatively, you could modify the code and the build process to
remove the UCMRT from the experiments and use the rest of the
interface. In case of questions, please contact the first author of
the paper.

The code depends on a server to run in order to send data from the
experiment. You can modify `communicator.js` to work with your own
server or remove the code in the `Communicator` class methods to see
the interface run locally.

## References

Pahor A, Stavropoulos T, Jaeggi SM, Seitz AR. Validation of a matrix
reasoning task for mobile devices. Behav Res Methods. 2019
Oct;51(5):2256-2267. doi: 10.3758/s13428-018-1152-2. PMID: 30367386;
PMCID: PMC6486467.
