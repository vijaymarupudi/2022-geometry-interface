set -eux

#!/usr/bin/env bash

EXPERIMENT_NAME=visuospatial

npm run build

ssh server << EOF
  mkdir /websites/experiments.vijaymarupudi.com/${EXPERIMENT_NAME} -p
  mkdir /websites/experiments.vijaymarupudi.com/api -p
EOF
rsync dist/ "server:/websites/experiments.vijaymarupudi.com/${EXPERIMENT_NAME}" -aP

rsync ../experiments-server/ server:/websites/experiments.vijaymarupudi.com/api/ -aP

ssh server << EOF
  sudo systemctl restart experiments-flask-server.service
EOF
