## Consent Form


You are invited to participate in a research study of how people think visually and spatially. You were selected as a possible participant because (a) you are an undergraduate student at the University of Minnesota, (b) you are between 18-24 years of age, and (c) you have ACT or SAT scores on file with the registrar. Please read this form and ask any questions you may have before agreeing to be in the study.

This study is being conducted by Vijay Marupudi (graduate student, Educational Psychology Department) and Dr. Sashank Varma (Professor, Educational Psychology Department).

Background Information
----------------------

The purpose of this study is to investigate how people judge the similarities or differences among sets of images.

Procedures
----------

If you agree to be in this study, we would ask you to complete four computerized tasks: (1) a perceptual similarity task, (2) a perceptual difference task, (3) a spatial reasoning task, and a (4) a spatial problem solving task.

In the perceptual similarity task, you will view a target image that is composed of straight and curved lines and simple shapes. You will be asked to pick which of two candidate images is “most similar to” the target image.

The perceptual difference task is similar, except that will be asked to pick which of two candidate images is “most different from” the target image.

In the spatial reasoning task, you will be shown a target image depicting a complex three-dimensional shape made up of cubes. You will also be shown 4 candidate images, and be asked to pick the 2 candidate images that depict the same shape as the target image.

In the spatial problem solving task, you will be shown a 3x3 grid of 8 images and 1 empty cell. You will also be shown 8 candidate images. Each image will be a simple combination of lines and shapes. You will be asked to notice patterns across the images, and on this basis to pick which candidate image goes in the missing cell.

Each task will begin with a few practice trials to familiarize you with how the stimuli will be presented and how you should respond. You will be allowed to take short breaks periodically. 

The study will take approximately 40-45 minutes to complete. We will ask you to complete it in one continuous session (except for the short breaks).

Risks and Benefits of being in the Study
----------------------------------------

The study has no known risks. However, you might find some of the experimental activities frustrating. You can withdraw from this study at any time and receive partial payment for your time.

There are no expected benefits to participating in this study.

Compensation:
-------------

You will receive payment in the form of \$15 for participating in this
study. Your participation is entirely voluntary. You are free to
withdraw at any time, and if you do, you will receive partial payment in
the form of \$5.

Confidentiality
---------------

The records of this study will be kept private. In any sort of report we
might publish, we will not include any information that will make it
possible to identify a subject. Research records will be stored securely
and only researchers will have access to the records.

Voluntary Nature of the Study
-----------------------------

Participation in this study is voluntary. Your decision whether or not
to participate will not affect your current or future relations with the
University of Minnesota. If you decide to participate, you are free to
not answer any question or withdraw at any time without affecting those
relationships.

Contacts and Questions
----------------------

For questions about the research study or results, or about other
concerns, please contact the research team members:


Vijay Marupudi\
Ph.D. student\
612-636-6788\
marupudi@umn.edu

Dr. Sashank Varma\
Professor\
612-625-6718\
sashank@umn.edu

This research has been reviewed and approved by an Institutional Review
Board (IRB) within the Human Research Protections Program (HRPP). To
share feedback privately with the HRPP about your research experience,
call the Research Participants' Advocate Line at 612-625-1650 or go to
<http://www.irb.umn.edu/report.html>. You are encouraged to contact the HRPP if:

* Your questions, concerns, or complaints are not being answered by
* the research team.
* You cannot reach the research team.
* You want to talk to someone besides the research team.
* You have questions about your rights as a research participant.
* You want to get information or provide input about this research.

You will be given a copy of this information to keep for your records at
the end of the experiment, if you desire.

Statement of Consent
--------------------

I have read the above information. I have asked questions and have
received answers. I consent to participate in the study.

By signing this form, I also agree to give permission to the
investigators to obtain my standardized test scores -- all components of
my ACT and/or SAT scores -- maintained by the University of Minnesota

Full Name: <input name="name" required>

Participant number: <input name="participantNumber" required>

UMN Student ID (number): <input name="umnStudentId" required>

UMN Email: <input name="umnEmail" required>

Signature (type full name): <input name="signature" required>

Today's Date (mm/dd/yyyy): <input name="date" required>
