# Demographics Form

We are collecting demographic and background information about our participants. These data will be anonymous and confidential. They will not be reported at the individual level, but rather at the group level (e.g., “8% of the participants were left handed”).

Please choose the answer that best applies to you and/or write in your response on the blank line.

1. Are you Hispanic or Latin(x)?

    <div><input type="radio" name="hispanic" id="hispanicYes" value="Yes"><label for="hispanicYes">Yes</label></div>
<div><input type="radio" name="hispanic" id="hispanicNo" value="No"><label for="hispanicNo">No</label></div>


2. With which racial category or categories do you most closely identify:

    <div><input type="radio" name="racialCategories" id="racialCategoriesAmerican Indian / Alaskan Native" value="American Indian / Alaskan Native"><label for="racialCategoriesAmerican Indian / Alaskan Native">American Indian / Alaskan Native</label></div>
<div><input type="radio" name="racialCategories" id="racialCategoriesAsian" value="Asian"><label for="racialCategoriesAsian">Asian</label></div>
<div><input type="radio" name="racialCategories" id="racialCategoriesBlack or African American" value="Black or African American"><label for="racialCategoriesBlack or African American">Black or African American</label></div>
<div><input type="radio" name="racialCategories" id="racialCategoriesNative Hawaiian or Other Pacific Islander" value="Native Hawaiian or Other Pacific Islander"><label for="racialCategoriesNative Hawaiian or Other Pacific Islander">Native Hawaiian or Other Pacific Islander</label></div>
<div><input type="radio" name="racialCategories" id="racialCategoriesWhite" value="White"><label for="racialCategoriesWhite">White</label></div>
<div><input type="radio" name="racialCategories" id="racialCategoriesMultiracial" value="Multiracial"><label for="racialCategoriesMultiracial">Multiracial</label></div>


    If you picked "Multiracial", please note which categories you identify with: <input type="text" name="racialCategoriesMultiracial">

3. With which gender identity do you most closely identify:

    <div><input type="radio" name="gender" id="genderMale" value="Male"><label for="genderMale">Male</label></div>
<div><input type="radio" name="gender" id="genderFemale" value="Female"><label for="genderFemale">Female</label></div>
<div><input type="radio" name="gender" id="genderNon-binary or gender non-confirming" value="Non-binary or gender non-confirming"><label for="genderNon-binary or gender non-confirming">Non-binary or gender non-confirming</label></div>
<div><input type="radio" name="gender" id="genderPrefer not to say" value="Prefer not to say"><label for="genderPrefer not to say">Prefer not to say</label></div>
<div><input type="radio" name="gender" id="genderPrefer to self describe" value="Prefer to self describe"><label for="genderPrefer to self describe">Prefer to self describe</label></div>


    If you picked "Prefer to self describe", please describe here: <input type="text" name="genderSelfDescribe">

4. Are you left or right-handed?

    <div><input type="radio" name="handedness" id="handednessLeft-handed" value="Left-handed"><label for="handednessLeft-handed">Left-handed</label></div>
<div><input type="radio" name="handedness" id="handednessRight-handed" value="Right-handed"><label for="handednessRight-handed">Right-handed</label></div>


5. What is your academic year?

    <div><input type="radio" name="academicYear" id="academicYearFreshman" value="Freshman"><label for="academicYearFreshman">Freshman</label></div>
<div><input type="radio" name="academicYear" id="academicYearSophomore" value="Sophomore"><label for="academicYearSophomore">Sophomore</label></div>
<div><input type="radio" name="academicYear" id="academicYearJunior" value="Junior"><label for="academicYearJunior">Junior</label></div>
<div><input type="radio" name="academicYear" id="academicYearSenior" value="Senior"><label for="academicYearSenior">Senior</label></div>


6. What is your birthdate (mm/dd/yyyy)?

    <input type="text" name="birthdate">

7. What was the highest level of math that you took in high school?

    <div><input type="radio" name="mathLevel" id="mathLevelAlgebra" value="Algebra"><label for="mathLevelAlgebra">Algebra</label></div>
<div><input type="radio" name="mathLevel" id="mathLevelGeometry with an algebra prerequisite" value="Geometry with an algebra prerequisite"><label for="mathLevelGeometry with an algebra prerequisite">Geometry with an algebra prerequisite</label></div>
<div><input type="radio" name="mathLevel" id="mathLevelPre-calculus or trigonometry" value="Pre-calculus or trigonometry"><label for="mathLevelPre-calculus or trigonometry">Pre-calculus or trigonometry</label></div>
<div><input type="radio" name="mathLevel" id="mathLevelCalculus" value="Calculus"><label for="mathLevelCalculus">Calculus</label></div>
<div><input type="radio" name="mathLevel" id="mathLevelOther (please specify)" value="Other (please specify)"><label for="mathLevelOther (please specify)">Other (please specify)</label></div>


    If you picked "Other", please describe here: <input type="text" name="mathOtherDescribe">

8. What is your **college** major / minor? (Indicate more than one if applicable.)

    <input type="text" name="majorMinor">

11. Please list your college courses (designator, number, and short title) in **mathematics**

    <textarea name="mathCourses" cols="80" rows="20"></textarea>

