import { formPlugin } from './utils'
import { DEMOGRAPHICS_FORM } from './constants'

export function* demographicsPart() {
  yield formPlugin({ form: DEMOGRAPHICS_FORM })
}
