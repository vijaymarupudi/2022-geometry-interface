import * as RT from "./RT";

const { cxs, Stopwatch } = RT.plugins.utils;

export const formPlugin = RT.makePlugin("formPlugin", (screen, config, callback) => {
  const form = document.createElement("form");
  screen.appendChild(form);
  const div = document.createElement("div");
  div.style = "max-width: 1000px; margin: 0 auto;";
  form.appendChild(div);
  div.innerHTML = config.form;

  form.addEventListener("submit", e => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const ret = {};
    for (const [key, value] of formData.entries()) {
      ret[key] = value;
    }
    callback(ret);
  });
});

export function spaceToRespond(text, normal = false) {
  const spacebar = "<p>Press [spacebar] to continue.</p>";
  const html = normal
    ? `<div class="${cxs({
        maxWidth: "34em",
        margin: "0 auto",
        textAlign: "left"
      })}">${text}${spacebar}</div>`
    : text + spacebar;

  return RT.plugins.jsPsych({
    type: "html-keyboard-response",
    stimulus: html,
    choices: [32]
  });
}

export async function get(url) {
  const x = await fetch(url);
  if (!x.ok) {
    throw new Error("url fetching failed");
  }
  return x.json();
}

function cacheImage(imageFilename) {
  return new Promise(resolve => {
    const image = new Image();
    image.addEventListener("load", () => {
      resolve();
    });
    image.src = imageFilename;
  });
}

export function cacheImages(paths) {
  return Promise.all(paths.map(filename => cacheImage(filename)));
}

export class Selection {
  constructor(capacity) {
    this._log = [];
    this.capacity = capacity;
    this.watch = new Stopwatch();
  }

  state() {
    if (this.valid()) {
      return this._log.slice(
        this._log.length - this.capacity,
        this._log.length
      );
    }
    return this._log;
  }

  start() {
    this.watch.start();
  }

  add(item) {
    if (this.in(item)) {
      return;
    }

    this._log.push({ ...this.watch.stop(), choice: item });
  }

  in(query) {
    return this.state()
      .map(x => x.choice)
      .includes(query);
  }

  valid() {
    return this._log.length >= this.capacity;
  }

  log() {
    return this._log;
  }
}
