import * as RT from "@vijaymarupudi/reaction-time";
import { CONSENT_FORM } from "./constants";
import { spaceToRespond, formPlugin } from "./utils";
import { communicator } from "./communicator";

function filterInt(value) {
  if (/^[-+]?(\d+|Infinity)$/.test(value)) {
    return Number(value);
  } else {
    return NaN;
  }
}

export async function* consentFormPart() {
  RT.plugins.jsPsych.init();
  while (true) {
    const trialData = yield formPlugin({ form: CONSENT_FORM });
    if (!filterInt(trialData.participantNumber)) {
      yield spaceToRespond(
        "<p>Your participant number was not valid. Please try again.</p>"
      );
    } else {
      try {
        communicator.participantNumber = trialData.participantNumber
        await communicator.consent(trialData);
      } catch (e) {
        document.body.innerHTML =
          "<h2>An error has occured</h2><p>Please contact the experimentor.</p>";
      }

      yield spaceToRespond("<p>Thank you!</p>");
      break;
    }
  }
}
