#!/usr/bin/bash



trap 'kill $(jobs -p)' INT

npm run dev &
cd ~/techno/development/work/experiments-server/
FLASK_ENV=debug poetry run flask run &


wait
