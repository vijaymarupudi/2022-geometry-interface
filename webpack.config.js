const CopyPlugin = require("copy-webpack-plugin");
module.exports = {
  mode: "development",
  entry: "./index.js",
  output: { filename: "bundle.js" },
  devServer: {
    contentBase: "./dist",
    proxy: {
      "/api": "http://localhost:5000"
    }
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "geometry_stimuli", to: "geometry_stimuli" },
        { from: "rotation_stimuli", to: "rotation_stimuli" },
        { from: "mrt_stimuli", to: "mrt_stimuli" },
        { from: "index.html", to: "index.html" },
      ],
    }),
  ],
};
