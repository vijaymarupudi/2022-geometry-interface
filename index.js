import * as RT from "./RT";
import { geometryExperiment } from "./geometryExperiment";
import { rotationExperiment } from "./rotationExperiment";
import { mrtExperiment } from "./mrtExperiment";
import { consentFormPart } from "./consentFormPart";
import { demographicsPart } from "./demographics";
import { spaceToRespond } from "./utils";
import { communicator } from "./communicator";

RT.plugins.jsPsych.init();

const seq = new RT.Sequence({
  onPluginFinish: trial => {
    console.log(trial);
    communicator.log(trial.pluginIndex);
  }
});

seq.init(async function*() {


  yield* consentFormPart();
  yield* geometryExperiment();
  yield spaceToRespond(
    "We will now move on to a different task. Please take a short break if needed. This is task <b>2/3</b>."
  );

  yield* rotationExperiment();
  yield spaceToRespond(
    "We will now move on to a different task. Please take a short break if needed. This is task <b>3/3</b>. We appreciate your effort in completing this task!"
  );

  yield* mrtExperiment();
  yield spaceToRespond(
    "Thank you for completing the task. Please complete the following demographics form."
  );
  yield* demographicsPart();

  try {
    await communicator.save(seq.data);
  } catch (e) {
    document.body.innerHTML =
      "<h2>An error has occured</h2><p>Please contact the experimentor</p>";
    throw e;
  }

  yield spaceToRespond(
    `
  <p>Thank you for participating in this study, your data has been uploaded. Please contact the experiment to receive your gift card.</p>
  `,
    true
  );
});
