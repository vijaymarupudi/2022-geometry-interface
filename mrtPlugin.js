import * as RT from "./RT";
import { Selection } from "./utils";

const { m, cxs } = RT.plugins.utils;

export const mrtPlugin = RT.makePlugin("matrix-reasoning-task", function(
  rootScreen,
  config,
  callback
) {
  const { stimulus } = config;

  const p = x => "mrt_stimuli/" + x;

  const screen = RT.centeredScreen(rootScreen);

  const MRT = () => {
    const selection = new Selection(1);

    return {
      oncreate() {
        selection.start()
      },
      view() {
        return m("div", [
          m("div", { class: cxs({ display: "flex", flexDirection: 'column' }) }, [
            m("img", {
              src: p(stimulus.target),
              class: cxs({ height: "60vh" })
            }),
            m(
              "div", { class: cxs({ display: "flex", flexWrap: 'wrap', width: '60vh' }) },
              stimulus.choices.map(img =>
                m(
                  "div",
                  {
                    class: cxs({
                      height: (60 / 4).toString() + "vh",
                      padding: "5px",
                      boxSizing: "border-box",
                      ...(selection.in(img) && { backgroundColor: "#002366" })
                    })
                  },
                  m("img", {
                    src: p(img),
                    height: "100%",
                    onclick: () => {
                      selection.add(img);
                    },
                    class: cxs({ cursor: "pointer" })
                  })
                )
              )
            )
          ]),
          m(
            "div",
            { class: cxs({ display: "flex", justifyContent: "center" }) },
            m(
              "button",
              {
                onclick: () => {
                  if (selection.valid()) {
                    m.cleanup(screen);

                    // there should only be one choice
                    const pickedImageName = selection.state()[0].choice;
                    const pickedImageChoiceIndex = stimulus.choices.findIndex(
                      x => x == pickedImageName
                    );

                    if (pickedImageChoiceIndex === -1) {
                      throw 'image not found in stimulus.choices. This shouldn\'t happen';
                    }

                    callback({
                      choice: pickedImageName,
                      choiceNumber: pickedImageChoiceIndex + 1,
                      log: selection.log(),
                      ...selection.watch.stop()
                    });
                  }
                },
                class: cxs({
                  margin: "15px 0",
                  ...(!selection.valid() && { visibility: "hidden" })
                })
              },
              "Continue"
            )
          )
        ]);
      }
    };
  };
  m.mount(screen, MRT);
});
