import { rotationPlugin } from "./rotationPlugin";
import { get, cacheImages, spaceToRespond } from "./utils";

function shuffleItemSet(set) {
  set = jsPsych.randomization.shuffle(set);
  return set.map(x => ({
    ...x,
    choices: jsPsych.randomization.shuffle(x.choices)
  }));
}

function isCorrect(trialData, item) {
  const participantsAnswers = trialData.choices.map(x => x.choice);
  participantsAnswers.sort();
  const correctAnswers = item.answers.map(x => x);
  correctAnswers.sort();

  let correct = true;
  for (let i = 0; i < participantsAnswers.length; i++) {
    if (!(participantsAnswers[i] === correctAnswers[i])) {
      correct = false;
    }
  }
  return correct;
}


const INSTRUCTIONS = `
<h2 id="instructions">Instructions</h2>
<p>For the upcoming trials, you will see a target block in the center of the screen. A sample target block is shown below.</p>
<p><img src="rotation_stimuli/training1_target.png" /></p>
<p>Below the object you will see 4 other choices of blocks. <strong>Of the 4 choices, 2 are the same as the target block, rotated.</strong> The other choices are different blocks altogether. Your task is to pick among the 4 choices the 2 that are the same as the target block.</p>
<p>When you have made your 2 choices, click the Continue button to respond and complete the trial.</p>
<p>We will now move to practice trials. You will be provided feedback during the practice trials. <strong>You will not receive feedback in the experimental trials.</strong>.</p>
`

// ## Instructions

// For the upcoming trials, you will see a target block in the center of
// the screen. A sample target block is shown below.

// ![](rotation_stimuli/training1_target.png)

// Below the object you will see 4 other choices of blocks. **Of the 4 choices,
// 2 are the same as the target block, rotated.** The other choices are
// different blocks altogether. Your task is to pick among the 4 choices the 2
// that are the same as the target block.
//
//
// When you have made your 2 choices, click the Continue button to respond and
// complete the trial.
//
// We will now move to practice trials. You will be provided feedback during
// the practice trials. **You will not receive feedback in the experimental
// trials.**.
//
export async function* rotationExperiment() { const stimuli = await
    get("rotation_stimuli/rotation_stimuli.json");

  const filenames = [...stimuli.training, ...stimuli.experimental]
    .flatMap(x => [x.target_img, ...x.choices, ...x.answers])
    .map(x => "rotation_stimuli/" + x);

  await cacheImages(filenames);

  const trainingItems = shuffleItemSet(stimuli.training);
  const experimentalItems = shuffleItemSet(stimuli.experimental);


  yield spaceToRespond(INSTRUCTIONS, true)

  yield spaceToRespond("<p>We will now move on to practice trials</p>");

  function nextTrialMsg() {
    return spaceToRespond("<p>Start the trial.</p>");
  }

  for (const item of trainingItems) {
    while (true) {
      yield nextTrialMsg();
      const trialData = yield rotationPlugin({
        stimulus: item
      });

      if (!isCorrect(trialData, item)) {
        yield spaceToRespond(
          "<p>That was <span style='color: red; font-weight: bold;'>incorrect!</span> Please try again!<p>"
        );
      } else {
        yield spaceToRespond(
          "<p>That was <span style='color: green; font-weight: bold;'>correct!</span> Moving on to the next practice trial.<p>"
        );
        break;
      }
    }
  }

  yield spaceToRespond("<p>We will now move on to experimental trials.</p>");

  for (const item of experimentalItems) {
    yield nextTrialMsg();
    yield rotationPlugin({
      stimulus: item
    });
  }

}
