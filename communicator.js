import ky from "ky";

function getNavigatorData() {
  const _navigator = {};
  for (const i in navigator) {
    _navigator[i] = navigator[i];
  }
  return _navigator;
}

class Communicator {
  constructor(name) {
    this.name = name;
    this.participantNumber = "UNKNOWN";
  }

  log(message) {
    return ky.post(`/api/${this.name}/log`, {
      json: {
        participantNumber: this.participantNumber,
        message
      }
    });
  }

  consent(consentResponse) {
    return ky.post(`/api/${this.name}/consent`, {
      json: {
        participantNumber: this.participantNumber,
        data: consentResponse
      }
    });
  }

  save(data) {
    return ky.post(`/api/${this.name}/save`, {
      json: {
        participantNumber: this.participantNumber,
        data: {
          experimentData: data,
          navigatorData: getNavigatorData()
        }
      }
    });
  }
}

export const communicator = new Communicator("visuospatial");
